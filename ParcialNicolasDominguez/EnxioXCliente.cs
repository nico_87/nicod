﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcialNicolasDominguez
{
    public class EnvioXCliente
    {
        public string Nombre { get; set; }
        public DateTime Ingreso { get; set; }
        public bool Entregado { get; set; }

    }
}
