﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcialNicolasDominguez
{
    public class Prioritario: Envio
    {
        public override double CalcularCostoEnvio(int pesoKg, int volumenPaquete, bool ciudad)// $120 para la misma ciudad, $180 para distinta ciudad, a su vez si la carga pesa más de 5kg se añade $12,50 por cada kilo excedente (carga de 7kg añade $25).
        {
            double adicional = 0;
            if (pesoKg>5)
            {
                adicional= 12.5 * (pesoKg - 5;)
            }
            if (ciudad = true)
            {
                return 120 + adicional;
            }
            return 180 + adicional;
        }

        public override DateTime CalcularEntrega(DateTime fechaIngresaria, bool ciudad, int pesoKg)
        {
            int adicionalPorPeso = 5; int diasAdicional = 0;
            
            if (pesoKg>adicionalPorPeso)
            {
                diasAdicional = pesoKg - adicionalPorPeso;
            }

            if (ciudad == true)
            {
                return fechaIngresaria.AddDays(diasAdicional);
            }
            return fechaIngresaria.AddDays(diasAdicional + 1);
           
        }
    }
}
/*envíos prioritarios: se entregan en el día si es en la misma ciudad, o al día siguiente si es en otra ciudad.
 * Si la carga pesa más de 5kg se añade 1 día por cada kilogramo adicional (ejemplo 7kg para misma ciudad son 3 días de entrega y 4 días para otra ciudad).
