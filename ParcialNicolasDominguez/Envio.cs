﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcialNicolasDominguez
{
    public abstract class Envio
    {
        public int CodigoPostal { get; set; }
        public int DestinoPostal { get; set; }
        public int CodSeguimiento { get; set; }
        public int Volumen { get; set; }
        public int PesoKg { get; set; }
        public DateTime Ingreso { get; set; }
        public DateTime Entrega { get; set; }
        public string NombreCte { get; set; }
        public double Costo { get; set; }

        //ESTE METODO DEBERIA SER 1 SOLO
        public abstract double CalcularCostoEnvio(int pesoKg, int volumenPaquete, bool ciudad);
        public abstract DateTime CalcularEntrega(DateTime fechaIngresaria, bool ciudad, int pesoKg);
      



    }
}
