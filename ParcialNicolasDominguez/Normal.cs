﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcialNicolasDominguez
{
    public class Normal : Envio
    {
        public override double CalcularCostoEnvio(int pesoKg, int volumenPaquete, bool ciudad)
        {
            double costo = 0;
            costo = 10.25 * volumenPaquete + 5.75 * pesoKg;
            return costo;
        }

        

        public override DateTime CalcularEntrega(DateTime fechaIngresaria, bool ciudad, int pesoKg)
        {
            return fechaIngresaria.AddDays(3);
        }
    }
}
